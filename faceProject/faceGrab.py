import cv2
import numpy as np 
import os 
import sys


# Append more people if you want to
people = ['Aarohi', 'Krishna', 'Cynthiya', 'Devangi']

# Define the capture element
cap = cv2.VideoCapture(0)

# Define input batch size (number of samples)
batch_size_closeUP = 250
batch_size_far = 500
batch_size_chageBKclose = 750
batch_size_changeBKfar = 1000

# initialize the image number with 1
image_no = 1

# initialize the haar to crop the face directly
face_cascade = cv2.CascadeClassifier('/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml')


# Helper function to help crop the "face only"
def cropFace(im):

	faces = face_cascade.detectMultiScale(im, 1.3, 5)
	for (x,y,w,h) in faces:
		roi_color = im[y:y+h, x:x+w]
		
	return roi_color

for name in people:
	directory = os.getcwd() + '/' + str(name)
	# print for confirmation
	
	print(directory)

	# Make the directory is already not present
	if not os.path.exists(directory):
		os.makedirs(directory)

	# Save the image in configuration 1
	print "Normal Mode, close up shots, press ENTER to continue"
	x = raw_input()
	while (image_no < batch_size_closeUP):
		
		_, frame = cap.read()
		try:
			frame = cropFace(frame)
			cv2.imwrite(directory + '/' + str(image_no) + '.jpg', frame)
		except: pass
		image_no += 1

        # Save the image in configuration 2
        print "Normal Mode, far shots, press ENTER to continue"
	x = raw_input()

	while (image_no < batch_size_far):
		_, frame = cap.read()
		try:
			frame = cropFace(frame)
                	cv2.imwrite(directory + '/' + str(image_no) + '.jpg', frame)
		except: pass
                image_no += 1

        # Save the image in configuration 3
        print "Normal Mode, change bk, press ENTER to continue"
	x = raw_input()

        while (image_no < batch_size_chageBKclose):
                _, frame = cap.read()
	
		try:
			frame = cropFace(frame)
                	cv2.imwrite(directory + '/' + str(image_no) + '.jpg', frame)
		except: pass
                image_no += 1

        # Save the image in configuration 1
        print "Normal Mode, far change bk, press ENTER to continue"

        while (image_no < batch_size_changeBKfar):
                _, frame = cap.read()
		try:
			frame = cropFace(frame)
                	cv2.imwrite(directory + '/' + str(image_no) + '.jpg', frame)

		except: pass
                image_no += 1

	# Reset the image number to 1 for the next folder
	image_no = 1

	
