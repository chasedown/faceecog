# Scope and Guidelines

1. Clone this repo 

```$ git clone https://harshseq@bitbucket.org/chasedown/faceecog.git```

2. Navigate to faceProj

3. Run the code

4. There are 4 major shots; 250 frames each. A close up, a little bit far; A closeup with different background and a little bit far with the same background.

5. Keep changing your expressions while capturing frames.

6. Follow the process.
